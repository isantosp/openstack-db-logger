Name:  openstack-db-logger
Version: 3.2
Release: 1%{?dist}
Summary: Tool for accounting resources in openstack reading from the DBs
Source0: %{name}-%{version}.tar.gz
BuildArch: noarch
Group: CERN/Utilities
License: Apache License, Version 2.0
URL: https://gitlab.cern.ch/cloud-infrastructure/openstack-db-logger

%{?systemd_requires}
BuildRequires: systemd
BuildRequires: python-devel
Requires: openstack-db-common >= 0.0
Requires: python-kazoo
Requires: MySQL-python
Requires: python-sqlalchemy
Requires: python-keystoneclient

%description
Tool for accounting resources in openstack reading from the DBs

%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%{__install} -p -D -m 644 etc/dblogger/dblogger.conf.example \
    %{buildroot}%{_sysconfdir}/dblogger/dblogger.conf.example
%{__install} -p -D -m 644 etc/logrotate.d/dblogger \
    %{buildroot}%{_sysconfdir}/logrotate.d/dblogger
%{__install} -p -D -m 644 etc/systemd/dblogger.service %{buildroot}%{_unitdir}/dblogger.service
%{__install} -p -D -m 755 scripts/dblogger %{buildroot}/usr/bin/dblogger
%{__mkdir} -p %{buildroot}/var/log/dblogger/

%clean
%{__rm} -rf %{buildroot}

%post
%systemd_post dblogger.service

%preun
%systemd_preun dblogger.service

%postun
%systemd_postun_with_restart dblogger.service

%files
%defattr(-,root,root,-)
%{_bindir}/dblogger
%{python_sitelib}/*

%dir /etc/dblogger
%dir /var/log/dblogger
/etc/dblogger/dblogger.conf.example
/etc/logrotate.d/dblogger
%{_unitdir}/dblogger.service


%changelog
* Mon Dec 12 2016 Luis Pigueiras <luis.pigueiras@cern.ch> 3.2-1
- [OS-3924] Added gb volume quota per type and project
- [OS-3912] Force dblogger restart after update

* Thu Dec 08 2016 Luis Pigueiras <luis.pigueiras@cern.ch> 3.1-1
- [OS-3907] Fix created/deleted in last hour metric
- [OS-3721] Add manila share count
- [OS-3890] Include disabled hypervisors in hv metrics
- [OS-3867] Include quota usages per project
- Fix comment with correct SQL query in magnum producer
- Add information about number of nodes per coe in magnum

* Fri Dec 02 2016 Luis Pigueiras <luis.pigueiras@cern.ch> 3.0-1
- Make code flake8 compliant
- Add gitlabci job
- Improve spec file to include dependencies and systemd unit file
- Add ZK variable to specify different paths
- Fix nova cell producer error when no data in a cell
- Disable Flume consumer
- Change format of produced metrics by component

* Fri Aug 26 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> 2.1-1
- Added Keystone producer
- Added Glance producer
- Added InfluxDB consumer
- New version based on Stefano's code

* Tue May 26 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-7
- Added VMs creation/deletion per tenants function (OS-1486)

* Mon May 04 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-5
- Improved dblogger logrotation (OS-1220) and refactored codes

* Thu Jan 29 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-4
- Isolated Keystone exception handling

* Thu Jan 08 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-2
- Added catching exceptions function to dblogger

* Fri Oct 10 2014 Wataru Takase <wataru.takase@cern.ch> 0.0-1
- First version
