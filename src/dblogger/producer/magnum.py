import logging

from sqlalchemy import func

from dblogger.producer.base import Base
from dblogger.producer.base import produce_metric

from dblogger.storage.sql import SQL

logger = logging.getLogger("magnum")


class Magnum(Base):

    def __init__(self, queue, section, configs):
        super(Magnum, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        # Create table mapping
        self.Cluster = self.db.map_table('cluster')
        self.ClusterTemplate = self.db.map_table('cluster_template')

    def run(self):
        # Total number of clusters
        num_clusters = produce_metric(metric="container_clusters_total",
                                      values={"count": self.cluster_num()})

        self.queue.put_nowait(num_clusters)

        # Number of nodes per orchestration engine
        for coe, num_nodes in self.cluster_nodes_per_coe():
            num_nodes_per_coe = produce_metric(
                metric="container_nodes_per_orchestration_engine",
                headers={"engine": coe},
                values={"count": num_nodes}
            )

            self.queue.put_nowait(num_nodes_per_coe)

        # Number of clusters per orchestration engine
        for coe, num in self.cluster_coe():
            num_clusters_per_coe = produce_metric(
                metric="container_clusters_per_orchestration_engine",
                headers={"engine": coe},
                values={"count": num})

            self.queue.put_nowait(num_clusters_per_coe)

        # Total number of nodes in cluster
        num_nodes = produce_metric(
            metric="container_nodes_total",
            values={"count": self.cluster_nodes()})
        self.queue.put_nowait(num_nodes)

    def cluster_num(self):
        # SELECT COUNT(id) FROM cluster;
        return self.db.query(
            func.count(self.Cluster.id).label("count")). \
            scalar()

    def cluster_coe(self):
        # SELECT cluster_template.coe, COUNT(cluster.id)
        #   FROM cluster_template INNER JOIN cluster
        #     ON cluster.cluster_template_id = cluster_template.uuid
        #   GROUP BY cluster_template.coe;
        return self.db.query(
            self.ClusterTemplate.coe,
            func.count(self.Cluster.id).label("count")) \
            .join(self.Cluster,
                  self.Cluster.cluster_template_id
                  == self.ClusterTemplate.uuid) \
            .group_by(self.ClusterTemplate.coe).all()

    def cluster_nodes(self):
        # SELECT SUM(node_count) FROM cluster;
        return self.db.query(
            func.sum(self.Cluster.node_count).label("total_nodes")). \
            scalar()

    def cluster_nodes_per_coe(self):
        # SELECT cluster_template.coe, SUM(cluster.node_count)
        #   FROM cluster
        #   INNER JOIN cluster_template
        #     ON cluster.cluster_template_id = cluster_template.uuid
        #   WHERE cluster.status IN ('CREATE_COMPLETE', 'UPDATE_COMPLETE')
        #   GROUP BY cluster_template.coe;
        return self.db.query(
                self.ClusterTemplate.coe,  # noqa
                func.sum(self.Cluster.node_count).label("nodes")
            ) \
            .join(self.Cluster,
                  self.Cluster.cluster_template_id ==
                      self.ClusterTemplate.uuid) \
            .filter(self.Cluster.status.in_(('CREATE_COMPLETE',
                                             'UPDATE_COMPLETE'))) \
            .group_by(self.ClusterTemplate.coe).all()
