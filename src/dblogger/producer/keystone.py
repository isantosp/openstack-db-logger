from sqlalchemy import func

from dblogger.producer.base import Base
from dblogger.producer.base import produce_metric

from dblogger.storage.sql import SQL


class Keystone(Base):

    def __init__(self, queue, section, configs):
        super(Keystone, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        # Create table mapping
        self.Project = self.db.map_table('project')
        self.User = self.db.map_table('user')

    def run(self):
        # Generate keystone generic data
        users = produce_metric(metric="users_total", values={
            "count": self.user_count()
        })

        projects = produce_metric(metric="projects_total", values={
            "count": self.project_count()
        })

        self.queue.put_nowait(users)
        self.queue.put_nowait(projects)

    def user_count(self):
        # SELECT COUNT(id) FROM user WHERE enabled=True;
        return self.db.query(
            func.count(self.User.id).label("count")). \
            filter(self.User.enabled == True).scalar()  # noqa

    def project_count(self):
        # SELECT COUNT(id) FROM project WHERE enabled=True;
        return self.db.query(
            func.count(self.Project.id).label("count")). \
            filter(self.Project.enabled == True).scalar()  # noqa
