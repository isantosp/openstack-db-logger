from sqlalchemy import func

from dblogger.producer.base import produce_metric
from dblogger.producer.nova import Nova


class NovaCell(Nova):
    def __init__(self, queue, section, configs):
        super(NovaCell, self).__init__(queue, section, configs)

    def run(self):
        hypervisors_total = produce_metric(
            metric="hypervisors_per_cell",
            headers={"cell": self.configs.get(self.section, 'name')},
            values={
                "disabled": self.hv_disabled(),
                "total": self.hv_total()
            })

        usage = self.hv_usage()
        hypervisors_ram = produce_metric(
            metric="hypervisors_ram_per_cell",
            headers={"cell": self.configs.get(self.section, 'name')},
            values={
                "total": usage['hv_ram_total'],
                "used": usage['hv_ram_used'],
            })
        hypervisors_cpu = produce_metric(
            metric="hypervisors_cpu_per_cell",
            headers={"cell": self.configs.get(self.section, 'name')},
            values={
                "total": usage['hv_cpu_total'],
                "used": usage['hv_cpu_used'],
            })
        hypervisors_disk = produce_metric(
            metric="hypervisors_disk_per_cell",
            headers={"cell": self.configs.get(self.section, 'name')},
            values={
                "total": usage['hv_disk_total'],
                "used": usage['hv_disk_used'],
            })
        vm_total = produce_metric(
            metric="vms_per_cell",
            headers={"cell": self.configs.get(self.section, 'name')},
            values={
                "count": self.vm_total()
            })

        created, deleted = self.vm_created_deleted_last_hour()

        vm_changes_in_last_hour = produce_metric(
            metric="vms_changes_per_cell",
            headers={"cell": self.configs.get(self.section, 'name')},
            values={
                "created_in_last_hour": created,
                "deleted_in_last_hour": deleted,
            })

        for count, status in self.vms_per_status():
            vms = produce_metric(
                metric="vms_per_status_per_cell",
                headers={
                    "cell": self.configs.get(self.section, 'name'),
                    "status": status
                },
                values={
                    "count": count
                }
            )

            self.queue.put_nowait(vms)

        self.queue.put_nowait(hypervisors_total)
        self.queue.put_nowait(hypervisors_cpu)
        self.queue.put_nowait(hypervisors_ram)
        self.queue.put_nowait(hypervisors_disk)
        self.queue.put_nowait(vm_total)
        self.queue.put_nowait(vm_changes_in_last_hour)

    def hv_total(self):
        # SELECT COUNT(*) FROM nova.services WHERE topic = "compute"
        # AND NOT deleted;
        return (self.db.query(
                func.count(self.Service.id).label("hv_total"))  # noqa: E126
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0).scalar())

    def hv_disabled(self):
        # SELECT COUNT(*) FROM nova.services
        # WHERE topic = "compute" AND NOT deleted AND disabled > 0;
        return (self.db.query(
                func.count(self.Service.id).label("hv_disabled"))  # noqa: E126
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0)
            .filter(self.Service.disabled > 0).scalar())

    def hv_usage(self):
        hv_usage = {}

        # hv_cpu_total
        # hv_cpu_used
        # hv_ram_total
        # hv_ram_used
        # hv_disk_total
        # hv_disk_used
        # SELECT SUM(vcpus) AS vcpu, SUM(vcpus_used) AS vcpu_used,
        #        SUM(memory_mb) AS ram, SUM(memory_mb_used) AS ram_used,
        #        SUM(local_gb) AS disk, SUM(local_gb_used) AS disk_used
        # FROM nova.compute_nodes WHERE hypervisor_hostname IN
        #     (SELECT host FROM nova.services
        #      WHERE topic = "compute" AND NOT deleted);
        hv_tmp = (self.db.query(
                func.sum(self.ComputeNode.vcpus).label("hv_cpu_total"),  # noqa
                func.sum(self.ComputeNode.vcpus_used).label("hv_cpu_used"),
                func.sum(self.ComputeNode.memory_mb).label("hv_ram_total"),
                func.sum(self.ComputeNode.memory_mb_used).label("hv_ram_used"),
                func.sum(self.ComputeNode.local_gb).label("hv_disk_total"),
                func.sum(self.ComputeNode.local_gb_used).label("hv_disk_used"),
            )
            .filter(self.ComputeNode.hypervisor_hostname.in_(
                self.db.query(self.Service.host)
                    .filter(self.Service.topic == "compute")
                    .filter(self.Service.deleted == 0))).one())

        hv_usage["hv_cpu_total"] = hv_tmp.hv_cpu_total or 0
        hv_usage["hv_cpu_used"] = hv_tmp.hv_cpu_used or 0
        hv_usage["hv_ram_total"] = hv_tmp.hv_ram_total or 0
        hv_usage["hv_ram_used"] = hv_tmp.hv_ram_used or 0
        hv_usage["hv_disk_total"] = hv_tmp.hv_disk_total or 0
        hv_usage["hv_disk_used"] = hv_tmp.hv_disk_used or 0

        return {k: int(v) for k, v in hv_usage.iteritems()}
