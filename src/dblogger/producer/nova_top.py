from dblogger.producer.base import produce_metric
from dblogger.producer.nova import Nova

from sqlalchemy import func
from sqlalchemy import and_


class NovaTop(Nova):

    def __init__(self, queue, section, configs):
        super(NovaTop, self).__init__(queue, section, configs)
        self.Quotas = self.db.map_table('quotas')
        self.QuotaUsages = self.db.map_table('quota_usages')

    def run(self):
        vm_total = produce_metric(
            metric="vms_count_per_cell",
            values={
                "count": self.vm_total()
            })

        created, deleted = self.vm_created_deleted_last_hour()

        vm_changes_in_last_hour = produce_metric(
            metric="vms_changes_total",
            values={
                "created_in_last_hour": created,
                "deleted_in_last_hour": deleted,
            })

        for count, status in self.vms_per_status():
            vms = produce_metric(
                metric="vms_per_status",
                headers={
                    "status": status
                },
                values={
                    "count": count
                }
            )

            self.queue.put_nowait(vms)

        for row in self.quotas_per_project():
            quota_limit = row.quota or 0
            quota_usage = row.usage or 0

            result = produce_metric(
                metric="{0}_quota_per_project".format(row.resource),
                headers={
                    'project_id': row.project_id
                },
                values={
                    "quota": float(quota_limit),
                    "usage": float(quota_usage)
                }
            )

            self.queue.put_nowait(result)

        for row in self.vms_per_status_per_project():
            result = produce_metric(
                metric="vms_per_status_per_project",
                headers={
                    "status": row.vm_state,
                    "project_id": row.project_id
                },
                values={
                    "count": row.count
                })

            self.queue.put_nowait(result)

        self.queue.put_nowait(vm_total)
        self.queue.put_nowait(vm_changes_in_last_hour)

    def quotas_per_project(self):
        # SELECT
        #   quotas.project_id,
        #   quotas.resource,
        #   quotas.hard_limit,
        #   SUM(quota_usages.in_use)
        # FROM quotas
        #    LEFT JOIN quota_usages
        #      ON quota_usages.project_id = quotas.project_id
        #         AND quota_usages.resource = quotas.resource
        # WHERE quotas.resource IN ('cores', 'instances', 'ram')
        # GROUP BY quotas.resource, quotas.project_id
        return (
            self.db.query(
                self.Quotas.project_id.label("project_id"),
                self.Quotas.resource.label("resource"),
                self.Quotas.hard_limit.label("quota"),
                func.sum(self.QuotaUsages.in_use).label("usage"),
            )
            .outerjoin(
                self.QuotaUsages,
                and_(
                    self.Quotas.project_id == self.QuotaUsages.project_id,
                    self.Quotas.resource == self.QuotaUsages.resource
                )
            )
            .filter(
                self.Quotas.resource.in_(('cores', 'instances', 'ram',))
            )
            .group_by(
                self.Quotas.resource,
                self.Quotas.project_id
            )
        )

    def vms_per_status_per_project(self):
        # SELECT count(id), vm_state, project_id
        # FROM instances
        # WHERE deleted = 0
        # GROUP BY vm_state, project_id
        return (
            self.db.query(
                func.count(self.Instance.id).label("count"),
                self.Instance.vm_state,
                self.Instance.project_id
            )
            .filter(self.Instance.deleted == 0)
            .group_by(
                self.Instance.vm_state,
                self.Instance.project_id
            )
        )
