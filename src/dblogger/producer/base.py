from abc import abstractmethod


class Base(object):

    def __init__(self, queue, section, configs):
        super(Base, self).__init__()
        self.queue = queue
        self.configs = configs
        self.section = section

    @abstractmethod
    def run(self):
        pass


def produce_metric(metric, headers=None, values=None):
    """Helper to produce metric that can be understood by consumers

    :args metric: Metric name
    :args headers: A dict with metadata for the metric
    :args values: A dict with actual values for the metric
    :returns: A dict with `header` and `values`
    """
    metric = {
        "header": {
            "metric": metric
        },
        "values": {
        }
    }

    if headers is not None:
        metric['header'].update(headers)
    if values is not None:
        metric['values'].update(values)

    return metric
