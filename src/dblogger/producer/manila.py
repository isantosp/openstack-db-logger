from sqlalchemy import func

from dblogger.producer.base import Base
from dblogger.producer.base import produce_metric

from dblogger.storage.sql import SQL


class Manila(Base):

    def __init__(self, queue, section, configs):
        super(Manila, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        self.Shares = self.db.map_table('shares')

    def run(self):
        shares_count = produce_metric(
            metric="fileshares_total",
            values={
                "count": self.shares_count()
            }
        )

        self.queue.put_nowait(shares_count)

        for row in self.shares_count_per_project():
            result = produce_metric(
                metric="fileshares_per_project",
                headers={
                    "project_id": row.project_id
                },
                values={
                    "count": row.count
                }
            )

            self.queue.put_nowait(result)

    def shares_count(self):
        # SELECT COUNT(id) FROM shares WHERE deleted = 0;
        return (
            self.db.query(
                func.count(self.Shares.id)
            )
            .filter(self.Shares.deleted == 0)
            .scalar()
        )

    def shares_count_per_project(self):
        # SELECT COUNT(id), project_id FROM shares WHERE deleted = 0
        # GROUP BY project_id;
        return (
            self.db.query(
                func.count(self.Shares.id).label("count"),
                self.Shares.project_id
            )
            .filter(self.Shares.deleted == 0)
            .group_by(self.Shares.project_id)
        )
