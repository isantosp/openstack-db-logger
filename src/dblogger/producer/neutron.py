from sqlalchemy import func

from dblogger.producer.base import Base
from dblogger.producer.base import produce_metric

from dblogger.storage.sql import SQL


class Neutron(Base):

    def __init__(self, queue, section, configs):
        super(Neutron, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        # Create table mapping
        self.Ports = self.db.map_table('ports')

    def run(self):
        # Ports number
        ports_num = produce_metric(
            metric="ports_total",
            values={"count": self.network_ports_num()}
        )

        self.queue.put_nowait(ports_num)

        for status, num in self.network_ports_status():
            ports_status = produce_metric(
                metric="ports_per_status",
                headers={'status': status},
                values={'count': num}
            )
            self.queue.put_nowait(ports_status)

    def network_ports_num(self):
        # SELECT COUNT(id) FROM ports;
        return self.db.query(
            func.count(self.Ports.id).label("count")). \
            scalar()

    def network_ports_status(self):
        # SELECT status, COUNT(id) FROM ports GROUP BY status;
        return self.db.query(
            self.Ports.status,
            func.count(self.Ports.id).label("count")). \
            group_by(self.Ports.status).all()
