from sqlalchemy import func

from dblogger.producer.base import Base
from dblogger.producer.base import produce_metric

from dblogger.storage.sql import SQL


class Glance(Base):

    def __init__(self, queue, section, configs):
        super(Glance, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        # Create table mapping
        self.Images = self.db.map_table('images')

    def run(self):
        # Generate glance generic data
        images = produce_metric(
            metric="images_total",
            values={
                "all": self.image_num(),
                "active": self.image_active_num(),
                "size": self.image_size(),
            })

        self.queue.put_nowait(images)

    def image_num(self):
        # SELECT COUNT(id) FROM images;
        return self.db.query(
            func.count(self.Images.id).label("count")). \
            scalar()

    def image_active_num(self):
        # SELECT COUNT(id) FROM images WHERE status='active';
        return self.db.query(
            func.count(self.Images.id).label("count")). \
            filter(self.Images.status == 'active').scalar()

    def image_size(self):
        # SELECT SUM(size) FROM images WHERE status='active';
        return self.db.query(
            func.sum(self.Images.size).label("size")). \
            filter(self.Images.status == 'active').scalar()
