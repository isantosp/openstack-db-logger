from sqlalchemy import and_
from sqlalchemy import func

from dblogger.producer.base import Base
from dblogger.producer.base import produce_metric

from dblogger.storage.sql import SQL


class Cinder(Base):

    def __init__(self, queue, section, configs):
        super(Cinder, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        # Create table mapping
        self.Quota = self.db.map_table('quotas')
        self.QuotaUsage = self.db.map_table('quota_usages')
        self.Volumes = self.db.map_table('volumes')
        self.VolumeTypes = self.db.map_table('volume_types')

    def run(self):
        # Generate cinder totals per type
        for v in self.volume_data():
            result = produce_metric(
                metric="volumes_per_type",
                headers={
                    "type": v.name
                },
                values={
                    "count": v.count,
                    "size": v.size,
                }
            )

            self.queue.put_nowait(result)

        # It needs some massaging to split it in different tables
        for row in self.quotas():
            quota_limit = row.quota_limit or 0
            quota_usage = row.quota_usage or 0

            if row.resource in ('volumes', 'snapshots', 'gigabytes'):
                if row.resource == 'volumes':
                    metric_name = 'volumes'
                elif row.resource == 'gigabytes':
                    metric_name = 'volumes_gb'
                elif row.resource == 'snapshots':
                    metric_name = 'snapshots'

                result = produce_metric(
                    metric="{0}_quota_per_project".format(metric_name),
                    headers={
                        'project_id': row.project_id
                    },
                    values={
                        "quota": quota_limit,
                        "usage": quota_usage,
                    }
                )
            elif row.resource.startswith('volumes_'):
                # We get the volume type from names like volumes_cpio1
                volume_type = row.resource.split('_')[1]
                result = produce_metric(
                    metric='volumes_quota_per_type_per_project',
                    headers={
                        'type': volume_type,
                        'project_id': row.project_id
                    },
                    values={
                        "quota": quota_limit,
                        "usage": quota_usage,
                    })
            elif row.resource.startswith('gigabytes_'):
                # We get the volume type from names like volumes_cpio1
                volume_type = row.resource.split('_')[1]
                result = produce_metric(
                    metric='volumes_gb_quota_per_type_per_project',
                    headers={
                        'type': volume_type,
                        'project_id': row.project_id
                    },
                    values={
                        "quota": quota_limit,
                        "usage": quota_usage,
                    })
            else:
                continue  # Ignore snapshot types and backups

            self.queue.put_nowait(result)

    def volume_data(self):
        # SELECT volume_types.name, COUNT(*), SUM(volumes.size)
        # FROM volumes
        # INNER JOIN volume_types
        #   ON volumes.volume_type_id = volume_types.id
        # WHERE volumes.deleted = 0
        # GROUP BY volumes.volume_type_id;
        tmp_data = self.db.query(
                func.count(self.Volumes.id).label("count"),  # noqa: E126
                func.sum(self.Volumes.size).label("size"),
                self.VolumeTypes.name.label("name")) \
            .outerjoin(self.VolumeTypes,
                       self.Volumes.volume_type_id == self.VolumeTypes.id) \
            .filter(self.Volumes.deleted == 0) \
            .group_by(self.Volumes.volume_type_id).all()

        return tmp_data

    def quotas(self):
        # SELECT quotas.project_id, quotas.resource, quotas.hard_limit,
        #        usages.in_use
        # FROM cinder.quotas AS quotas
        # INNER JOIN cinder.quota_usages AS usages
        #   ON quotas.project_id = usages.project_id
        #     AND quotas.resource = usages.resource
        # WHERE quotas.hard_limit != 0
        #   AND quotas.deleted = 0
        # ORDER BY quotas.project_id;

        tmp_data = self.db.query(
                self.Quota.project_id.label("project_id"),  # noqa: E126
                self.Quota.resource.label("resource"),
                self.Quota.hard_limit.label("quota_limit"),
                self.QuotaUsage.in_use.label("quota_usage")) \
            .outerjoin(self.QuotaUsage, and_(
                self.Quota.project_id == self.QuotaUsage.project_id,
                self.Quota.resource == self.QuotaUsage.resource)) \
            .filter(self.Quota.hard_limit > 0) \
            .filter(self.Quota.deleted == 0) \
            .order_by(self.Quota.project_id).all()

        return tmp_data
