from abc import abstractmethod
from datetime import datetime
from datetime import timedelta
from sqlalchemy import func

from dblogger.producer.base import Base

from dblogger.storage.sql import SQL


class Nova(Base):

    def __init__(self, queue, section, configs):
        super(Nova, self).__init__(queue, section, configs)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))
        # Create table mapping
        self.ComputeNode = self.db.map_table('compute_nodes')
        self.Service = self.db.map_table('services')
        self.Instance = self.db.map_table('instances')

    @abstractmethod
    def run(self):
        pass

    def vms_per_status(self):
        # SELECT COUNT(*), vm_state FROM nova.instances WHERE NOT deleted
        # GROUP BY vm_state;
        return self.db.query(
                func.count(self.Instance.id),  # noqa: E126
                self.Instance.vm_state) \
            .filter(self.Instance.deleted == 0) \
            .group_by(self.Instance.vm_state).all()

    def vm_total(self):
        # SELECT COUNT(*) FROM nova.instances WHERE NOT deleted;
        return (self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.deleted == 0).scalar())

    def vm_created_deleted_last_hour(self):
        # vm_created_last_hour
        # SELECT * FROM nova.instances WHERE created_at BETWEEN <start_date>
        # AND <end_date>;
        utcnow = datetime.utcnow()
        start_date = utcnow.replace(minute=0, second=0, microsecond=0) \
            - timedelta(hours=1)
        end_date = utcnow.replace(minute=59, second=59, microsecond=0) \
            - timedelta(hours=1)

        created = (self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.created_at.between(start_date, end_date))
            .scalar())

        # vm_deleted_last_hour
        # SELECT COUNT(*) FROM nova.instances WHERE deleted_at
        # BETWEEN <start_date> AND <end_date>;
        deleted = (self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.deleted_at.between(start_date, end_date))
            .scalar())

        return created, deleted
