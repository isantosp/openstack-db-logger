
import uuid

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker


class SQL(object):

    def __init__(self, connection):
        super(SQL, self).__init__()

        self.engine = create_engine(
            connection,
            pool_size=1,
            max_overflow=0,
            echo=False,
            echo_pool=False)

        self.metadata = MetaData(self.engine)

        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def map_table(self, table):
        tbl = Table(table, self.metadata, autoload=True)
        cls_name = "%s_%s" % (table, str(uuid.uuid4()))
        cls = type(cls_name, (object, ), {})
        mapper(cls, tbl)
        return cls

    def query(self, *args):
        return self.session.query(*args)
