
import logging
import signal
import sys
import time

from kazoo.client import KazooClient
from multiprocessing import Pool
from multiprocessing import Queue

# from dblogger.consumer.log import Log
from dblogger.consumer.influx import Influx

from dblogger.producer.cinder import Cinder
from dblogger.producer.glance import Glance
from dblogger.producer.keystone import Keystone
from dblogger.producer.magnum import Magnum
from dblogger.producer.manila import Manila
from dblogger.producer.neutron import Neutron
from dblogger.producer.nova_cell import NovaCell
from dblogger.producer.nova_top import NovaTop

logger = logging.getLogger("dblogger")


def init_producer(queue):
    run_producer.queue = queue


def run_producer(section, configs):
    try:
        if "cell_top" in section:
            NovaTop(run_producer.queue, section, configs).run()
        elif "cell" in section:
            NovaCell(run_producer.queue, section, configs).run()
        elif "keystone" in section:
            Keystone(run_producer.queue, section, configs).run()
        elif "cinder" in section:
            Cinder(run_producer.queue, section, configs).run()
        elif "glance" in section:
            Glance(run_producer.queue, section, configs).run()
        elif "magnum" in section:
            Magnum(run_producer.queue, section, configs).run()
        elif "neutron" in section:
            Neutron(run_producer.queue, section, configs).run()
        elif "manila" in section:
            Manila(run_producer.queue, section, configs).run()
    except Exception:
        logger.exception("Error retrieving sample")


class DBLogger(object):

    def __init__(self, configs):
        super(DBLogger, self).__init__()
        self.configs = configs

    def run(self):
        # Queue where the samples are stored
        logger.info("Creating queue")
        self.queue = Queue(maxsize=0)

        # Create a pool of producers
        logger.info("Creating producer pool")
        self.pool = Pool(
            processes=int(self.configs.get('dblogger', 'workers')),
            initializer=init_producer,
            initargs=(self.queue,))

        # Create a consumer
        logger.info("Creating consumer process")
        self.consumer = Influx(
            self.queue,
            self.configs.get('influxdb', 'host'),
            self.configs.get('influxdb', 'port'),
            self.configs.get('influxdb', 'user'),
            self.configs.get('influxdb', 'pass'),
            self.configs.get('influxdb', 'database')
        )

        # self.consumer = Log(self.queue)
        self.consumer.start()
        logger.info(self.consumer.pid)

        # Configure signal handling
        signal.signal(signal.SIGINT, self.terminate)
        signal.signal(signal.SIGTERM, self.terminate)
        signal.signal(signal.SIGQUIT, self.terminate)

        # Get list of sections
        sections = list(
            set(self.configs.sections()) -
            set(['dblogger', 'logging']))

        # Configure kazoo
        logger.info("Creating kazoo partitioner")
        self.kclient = KazooClient(
            hosts=self.configs.get('dblogger', 'zookeeper_hosts'),
            logger=logger)
        self.kclient.start()

        partitioner = self.kclient.SetPartitioner(
            path=self.configs.get('dblogger', 'zookeeper_path'),
            set=tuple(sections))

        # Run producers loop
        while True:
            if partitioner.failed:
                raise Exception("Lost or unable to acquire partition")
            elif partitioner.release:
                partitioner.release_set()
            elif partitioner.acquired:
                for section in partitioner:
                    logger.debug("Running polling task for %s" % section)
                    self.pool.apply_async(run_producer,
                                          args=(section, self.configs,))
                # Wait for next task run
                time.sleep(int(self.configs.get('dblogger', 'interval')))
            elif partitioner.allocating:
                partitioner.wait_for_acquire()

    def terminate(self, signal, frame):
        logger.info("Terminating kazoo partitioner")
        self.kclient.stop()

        logger.info("Closing the queue")
        self.queue.close()

        logger.info("Terminating consumer")
        self.consumer.terminate()

        logger.info("Terminating producer pool")
        self.pool.terminate()

        sys.exit(0)
