import logging

from dblogger.consumer.base import Base

logger = logging.getLogger("dblogger")


class Log(Base):

    def __init__(self, queue):
        super(Log, self).__init__(queue)

    def process(self, sample):
        logger.info("consumer.log - %s" % (sample,))
