import logging
import sys

from influxdb import InfluxDBClient

from dblogger.consumer.base import Base

logger = logging.getLogger("dblogger")


class Influx(Base):

    def __init__(self, queue, host, port, user, password, database):
        super(Influx, self).__init__(queue)
        self.client = self.__init_client(host, port, user, password, database)

    def __init_client(self, host, port, user, password, database):
        try:
            client = InfluxDBClient(host=host, port=port, username=user,
                                    password=password, ssl=True,
                                    database=database)
        except Exception:
            logger.exception("Error connecting to InfluxDB. Check "
                             "database existance and connection URL")
            sys.exit(-1)

        return client

    def process(self, sample):
        logger.debug("Arrived sample: %s" % sample)

        influx_metric = [{
            'measurement': sample['header']['metric'],
            'tags': {key: value for key, value in sample['header'].iteritems()
                     if key != 'metric'},  # Exclude 'metric' from columns
            'fields': sample['values']
        }]

        try:
            logger.debug("Inserting: %s" % influx_metric)
            self.client.write_points(influx_metric)
        except Exception:
            logger.exception("Error sending sample to influxdb")
