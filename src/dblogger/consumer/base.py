
import logging

from multiprocessing import Process

logger = logging.getLogger("dblogger")


class Base(Process):
    """docstring for Base"""
    def __init__(self, queue):
        super(Base, self).__init__()
        self.daemon = True
        self.queue = queue

    def run(self):
        # Main loop
        while True:
            try:
                sample = self.queue.get()
                self.process(sample)
            except Exception:
                logger.exception("Exception reading from the queue")

    def process(self, sample):
        pass
