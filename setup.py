#!/usr/bin/env python

from distutils.core import setup

setup(
    name='openstack-db-logger',
    version='3.2',
    description='Tool for sending OpenStack usage to Flume',
    author='Cloud Infrastructure Team',
    author_email='cloud-infrastructure-3rd-level@cern.ch',
    url='http://www.cern.ch/openstack',
    package_dir={'': 'src'},
    packages=['dblogger', 'dblogger/consumer', 'dblogger/producer',
              'dblogger/storage'],
    scripts=['scripts/dblogger'],
)
